#include <iostream>
#include "../include/hw.h"

HelloWorld::HelloWorld() {
    times = 1;
}

HelloWorld::HelloWorld(int _times) {
    times = _times;
}

bool HelloWorld::changeTimes(int new_times) {
    times = new_times;
    return true;
}

int HelloWorld::getTimes() {
    return times;
}

HelloWorld::~HelloWorld() {
    std::cout << "Bye~" << std::endl;
}
#ifndef HW_H_
#define HW_H_

class HelloWorld {
    private:
        int times;
    
    public:
        HelloWorld();
        HelloWorld(int times);
        ~HelloWorld();

    public:
        bool changeTimes(int new_times);
        int getTimes();
};

#endif
//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct CandyBar {
    string brand;
    double weight;
    long calories;
};
int main() {
    CandyBar snack = {
            "Mocha Munch",
            2.3,
            350,
    };
    cout << "The snack's brand is " << snack.brand << endl
        << "The snack's weight is " << snack.weight << endl
        << "The snack's calories is " << snack.calories;
}
//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct CandyBar {
    string brand;
    double weight;
    long calories;
};
int main() {
    CandyBar* cb = new CandyBar[3];
    cb[0].brand = "a";
    cb[1].brand = "b";
    cb[2].brand = "c";
    cout << cb[0].brand << endl << cb[1].brand << endl << cb[2].brand;
}

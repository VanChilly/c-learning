//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"
#include "array"
#define ll long long
using namespace std;

int main() {
    array<double, 3> a;
    double ss;
    for (int i = 0; i < 3; i++) {
        cout << "Enter score: ";
        cin >> a[i];
        ss += a[i];
    }
    cout << "count: " << a.size() << endl << "mean value: " << ss / a.size();
}

//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"
#include "string"

#define ll long long
using namespace std;

int main() {
    string firstName, lastName;
    cout << "Enter your first name: ";
    cin >> firstName;
    cout << "Enter your last name: ";
    cin >> lastName;
    cout << "Here's the information in a single string: " << lastName + ", " + firstName;
}

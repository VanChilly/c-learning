//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct Pizza {
    string name;
    double d;
    double weight;
};
int main() {
    Pizza* p = new Pizza;
    cout << "Enter pizza's diameter: ";
    cin >> p->d;
    cout << "Enter pizza's name: ";
    cin >> p->name;
    cout << "Enter Pizza's weight: ";
    cin >> p->weight;
    cout << "INFO:\n" << "name: " + p->name + "\n" << "diameter: " << p->d << endl << "weight: " << p->weight;
    delete p;
}
//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct Pizza {
    string name;
    double d;
    double weight;
};
int main() {
    Pizza little_pizza;
    cout << "Enter pizza company name: ";
    cin >> little_pizza.name;
    cout << "Enter pizza diameter: ";
    cin >> little_pizza.d;
    cout << "Enter pizza weight: ";
    cin >> little_pizza.weight;
    cout << "The pizza's info: " << endl << "company name: " << little_pizza.name << endl
        << "diameter: " << little_pizza.d << endl << "weight: " << little_pizza.weight;

}

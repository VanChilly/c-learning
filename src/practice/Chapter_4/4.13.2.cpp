//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"
#include "string"

#define ll long long
using namespace std;

int main() {
    string name, dessert;
    cout << "Enter your name:\n";
    getline(cin, name);
    cout << "Enter your favorite dessert:\n";
    getline(cin, dessert);
    cout << "I have some delicious " << dessert;
    cout << " for you, " << name << ".\n";
    return 0;
}

//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct CandyBar {
    string brand;
    double weight;
    long calories;
};
int main() {
    CandyBar array[3] = {
            {"a", 0.1, 10},
            {"b", 2.0, 11},
            {"c", 3.0, 22},
    };
    cout << array[0].brand << ' ' << array[0].weight << ' ' << array[0].calories << endl
        << array[1].brand << ' ' << array[1].weight << ' ' << array[1].calories << endl
        << array[2].brand << ' ' << array[2].weight << ' ' << array[2].calories;
}

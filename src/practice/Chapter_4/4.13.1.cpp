//
// Created by VanChilly on 2022/9/3.
//
#include "iostream"
#include "string"

#define ll long long
using namespace std;

int main() {
    char firstName[20], lastName[20], grade;
    int age;
    cout << "What is your first name? ";
    cin.getline(firstName, 20);
    cout << "What is your last name? ";
    cin.getline(lastName, 20);
    cout << "What letter grade do you deserve? ";
    cin >> grade;
    cout << "What is your age? ";
    cin >> age;
    cout << "Name: " << lastName << ", " << firstName << endl;
    cout << "Grade: " << grade << endl;
    cout << "Age: " << age << endl;

}

// File Name: 8.8.6.cc
// Created by VanChilly on 2022/9/15.
//
#include <cstring>
#include "iostream"

#define ll long long
using namespace std;

template <class T> T maxn(T *, int);
template <> char * maxn(char *[], int n);
int main() {
   int arr1[6] = {1, 2, 3, 4, 5, 1};
   double arr2[4] = {2, 4, 1, 2};
   int ans1 = maxn(arr1, 6);
   int ans2 = maxn(arr2, 4);
   cout << ans1 << " " << ans2 << endl;
   char *p[2] = {"nn", "mm1"};
   char * q = maxn(p, 2);
   cout << q;
}

template <class T> T maxn(T * arr, int n) {
    T mx = arr[0];
    for (int i = 1; i < n; i ++ ) {
        mx = mx > arr[i] ? mx : arr[i];
    }
    return mx;
}

template <> char * maxn(char *p[], int n) {
    int l = strlen(p[0]);
    char * q;
    for (int i = 1; i < n; i ++ ) {
        if (l < strlen(p[i])) {
            l = strlen(p[i]);
            q = p[i];
        }
    }
    return q;
}
// File Name: 8.8.1.cc
// Created by VanChilly on 2022/9/15.
//
#include "iostream"

#define ll long long
using namespace std;

void PrintChar(char * ch, int n = 0);
int main() {
    char ch[20];
    cin.getline(ch, 20);
    PrintChar(ch);
    PrintChar(ch, 1);
    PrintChar(ch, 2);
}

void PrintChar(char * ch, int n){
    if (n != 0) {
        cout << ch;
    }
}
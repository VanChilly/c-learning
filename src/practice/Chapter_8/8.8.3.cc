// File Name: 8.8.3.cc
// Created by VanChilly on 2022/9/15.
//
#include "iostream"
#include "cctype"

#define ll long long
using namespace std;

void TransformToUpper(string &);
int main() {
    cout << "Enter a string (q to quit): ";
    string s;
    getline(cin, s);
    while (s != "q") {
        string & ss = s;
        TransformToUpper(ss);
        cout << "Next string (q to quit): ";
        getline(cin, s);
    }
    cout << "Bye.";
}

void TransformToUpper(string & s){
    for (auto item : s) {
        putchar(toupper(item));
    }
    cout << endl;
}

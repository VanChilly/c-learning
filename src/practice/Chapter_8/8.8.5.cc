// File Name: 8.8.5.cc
// Created by VanChilly on 2022/9/15.
//
#include "iostream"

#define ll long long
using namespace std;

template <class T> T max5(T * arr);
int main() {
    int arr[] = {1, 2, 3, 4, 5};
    max5(arr);
    double arr1[] = {1.0, 2.0, 1, 3, 4};
    max5(arr1);
}

template <class T> T max5(T * arr) {
    T mx = arr[0];
    for (int i = 1; i < 5; i ++ ) {
        mx = mx > arr[i] ? mx : arr[i];
    }
    cout << mx << endl;
}
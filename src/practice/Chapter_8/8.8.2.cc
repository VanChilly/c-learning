// File Name: 8.8.2.cc
// Created by VanChilly on 2022/9/15.
//
#include <cstring>
#include "iostream"

#define ll long long
using namespace std;

struct CandyBar {
    char band[100];
    double weight;
    int calories;
};

void SetValues(CandyBar &,
               const char * = "Millennium Munch",
               double = 2.85, int = 350);
void ShowContents(const CandyBar &);
int main() {
    CandyBar cb = CandyBar{};
    CandyBar & cbb = cb;
    SetValues(cbb);
    ShowContents(cbb);
}

void SetValues(CandyBar & cb,
               const char * ch,
               double weight,
               int calories) {
    strcpy(cb.band, ch);
    cb.weight = weight;
    cb.calories = calories;
}

void ShowContents(const CandyBar & cb) {
    cout << "band: " << cb.band << endl;
    cout << "weight: " << cb.weight << endl;
    cout << "calories: " << cb.calories << endl;
}

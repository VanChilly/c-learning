// File Name: 8.8.4.cc
// Created by VanChilly on 2022/9/15.
//
#include <iostream>
using namespace std;
#include <cstring>

struct stringy {
    char * str; // points to a string
    int ct;     // length of string (not counting '\0')
};

void set(stringy &, const char *);
void show(const stringy &, int n = 1);
void show(const char *, int n = 1);
int main() {
    stringy beany{};
    char testing[] = "Reality isn't what it used to be.";

    set(beany, testing); // first argument is a reference,
                            // allocates space to hold copy of testing,
                            // sets str member of beany to point to the
                            // new block, copies testing to new block.
                            // and sets ct member of beany
    show(beany);
    show(beany, 2);
    testing[0] = 'D';
    testing[1] = 'u';
    show(testing);      // prints testing string once
    show(testing, 3);   // prints testing string thrice
    show("Done!");
    return 0;
}

void set(stringy & beany, const char * testing) {
    beany.str = new char[100];
    strcpy(beany.str, testing);
    beany.ct = int(strlen(beany.str));
}

void show(const stringy & beany, int n) {
    for (int i = 0; i < n; i ++ ) {
        cout << beany.str << endl;
    }
}

void show(const char * testing, int n) {
    for (int i = 0; i < n; i ++ ) {
        cout << testing << endl;
    }
}

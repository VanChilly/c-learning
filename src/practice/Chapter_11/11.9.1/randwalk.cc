// File Name: randwalk.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "cstdlib"
#include "ctime"
#include "vector.h"
#include "fstream"

#define ll long long
using namespace std;

int main() {
    using namespace std;
    using VECTOR::Vector;
    srand(time(0)); // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    ofstream fout;
    fout.open(R"(G:\MyCode\c-learning\src\practice\Chapter_11\11.9.1\save.txt)");
    unsigned long steps = 0;
    double target;
    double dstep;
    cout << "Enter target distance (q to quit): ";
    while (cin >> target) {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;
        fout << "Target Distance: " << target << ", " << "Step Size: " << dstep << endl;
        while (result.magval() < target) {
            fout << steps << ": ";
            fout << result << endl;
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            steps ++;
        }
        cout << "After " << steps << " steps, the subject "
                                     "has the following location: \n";
        cout << result << endl;
        result.polar_mode();
        cout << " or\n" << result << endl;
        cout << "Average outward distance per step = "
        << result.magval() / steps << endl;
        result.rect_mode();
        fout << "After " << steps << " steps, the subject "
                                     "has the following location: \n";
        fout << result << endl;
        result.polar_mode();
        fout << " or\n" << result << endl;
        fout << "Average outward distance per step = "
                << result.magval() / steps << endl;
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}

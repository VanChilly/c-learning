// File Name: stonewt.h
// Created by VanChilly on 2022/9/24.
//

#ifndef SRC_STONEWT_H
#define SRC_STONEWT_H

class Stonewt {
public:
    enum Mode {Stone, Pounds};
private:
    enum {Lbs_per_stn = 14};
    int stone;
    double pds_left;
    double pounds;
    Mode mode;
public:
    Stonewt(double lbs, Mode m=Stone);
    Stonewt(int stn, double lbs, Mode m=Stone);
    friend std::ostream & operator<<(std::ostream & os, const Stonewt & st);
    Stonewt operator+(const Stonewt & s) const;
    Stonewt operator-(const Stonewt & s) const;
    Stonewt operator*(double m) const;
    friend Stonewt operator*(double m, const Stonewt & s);
    void set_stn() { mode = Stone;}
    void set_pds() { mode = Pounds;}
    Stonewt();
    ~Stonewt();
};

#endif //SRC_STONEWT_H

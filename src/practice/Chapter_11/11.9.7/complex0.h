// File Name: complex0.h
// Created by VanChilly on 2022/9/25.
//

#ifndef SRC_COMPLEX0_H
#define SRC_COMPLEX0_H

class Complex {
private:
    double rn; // real number
    double in; // imaginary number
public:
    Complex() { rn = 0.0; in = 0.0;};
    Complex(double a, double b);
    Complex operator+(const Complex & c);
    Complex operator-(const Complex & c);
    Complex operator*(const Complex & c);
    Complex operator~();
    friend Complex operator*(double x, const Complex & c);
    friend bool operator>>(std::istream & is, Complex & c);
    friend std::ostream & operator<<(std::ostream & os, const Complex & c);
    ~Complex();
};
#endif //SRC_COMPLEX0_H

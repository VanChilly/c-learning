// File Name: complex0.cc
// Created by VanChilly on 2022/9/25.
//
#include "iostream"
#include "complex0.h"

Complex::Complex(double a, double b) {
    rn = a;
    in = b;
}

Complex Complex::operator+(const Complex &c) {
    Complex result;
    result.in = in + c.in;
    result.rn = rn + c.rn;
    return result;
}

Complex Complex::operator-(const Complex &c) {
    Complex result;
    result.in = in - c.in;
    result.rn = rn - c.rn;
    return result;
}

Complex Complex::operator*(const Complex &c) {
    Complex result;
    result.rn = rn * c.rn - in * c.in;
    result.in = rn * c.in + in * c.rn;
    return result;
}

Complex operator*(double x, const Complex & c) {
    Complex result;
    result.rn = x * c.rn;
    result.in = x * c.in;
    return result;
}

Complex Complex::operator~() {
    Complex result;
    result.rn = rn;
    result.in = -in;
    return result;
}

bool operator>>(std::istream & is, Complex & c) {
    int f1 = 0, f2 = 0;
    std::cout << "Real: ";
    if (is >> c.rn) {
        f1 = 1;
    };
    if (!f1) return false;
    std::cout << "Imaginary: ";
    if (is >> c.in) {
        f2 = 1;
    };
    return f2;
}

std::ostream & operator<<(std::ostream & os, const Complex & c) {
    os << "(" << c.rn << ", " << c.in << "i)" << std::endl;
}

Complex::~Complex() {}

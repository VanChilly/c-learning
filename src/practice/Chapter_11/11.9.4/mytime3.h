// File Name: mytime3.h
// Created by VanChilly on 2022/9/20.
//

// mytime3.h -- Time class with friends
#ifndef SRC_MYTIME3_H
#define SRC_MYTIME3_H
#include "ostream"
class Time {
private:
    int hours;
    int minutes;
public:
    Time();
    Time(int h, int m = 0);
    void AddMin(int m);
    void AddHr(int h);
    void Reset(int h = 0, int m = 0);
//    Time operator+(const Time & t) const;
//    Time operator-(const Time & t) const;
//    Time operator*(double n) const;
    friend Time operator+(const Time & t1, const Time & t2);
    friend Time operator-(const Time & t, const Time & t2);
    friend Time operator*(const Time & t, double m);
    friend Time operator*(double m, const Time & t);
    friend std::ostream & operator<<(std::ostream & os, const Time & t);
};

#endif //SRC_MYTIME3_H

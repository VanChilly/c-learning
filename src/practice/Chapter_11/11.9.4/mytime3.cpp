// File Name: mytime3.cpp
// Created by VanChilly on 2022/9/20.
//
#include "iostream"
#include "mytime3.h"

#define ll long long
using namespace std;

Time::Time() {
    hours = minutes = 0;
}

Time::Time(int h, int m) {
    hours = h;
    minutes = m;
}

void Time::AddMin(int m) {
    minutes += m;
    hours += minutes / 60;
    minutes %= 60;
}

void Time::AddHr(int h) {
    hours += h;
}

void Time::Reset(int h, int m) {
    hours = h;
    minutes = m;
}

Time operator+(const Time & t1, const Time & t2) {
    Time result;
    long totalMinutes = (t1.hours + t2.hours) * 60 + t1.minutes + t2.minutes;
    result.hours = totalMinutes / 60;
    result.minutes = totalMinutes % 60;
    return result;
}

Time operator-(const Time & t1, const Time & t2) {
    Time result;
    long totalMinutes = t1.hours * 60 + t1.minutes - t2.hours * 60 * t2.minutes;
    result.hours = totalMinutes / 60;
    result.minutes = totalMinutes % 60;
    return result;
}

Time operator*(const Time & t, double m) {
    Time result;
    long totalMinutes = (t.hours * 60 + t.minutes) * m;
    result.hours = totalMinutes / 60;
    result.minutes = totalMinutes % 60;
    return result;
}

Time operator*(double m, const Time & t) {
    Time result;
    long totalMinutes = (t.hours * 60 + t.minutes) * m;
    result.hours = totalMinutes / 60;
    result.minutes = totalMinutes % 60;
    return result;
}
std::ostream & operator<<(std::ostream & os, const Time & t) {
    os << t.hours << " hours, " << t.minutes << " minutes";
    return os;
}


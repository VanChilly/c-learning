// File Name: stonewt.cc
// Created by VanChilly on 2022/9/24.
//
#include "iostream"
#include "stonewt.h"

#define ll long long
using namespace std;

Stonewt::Stonewt(double lbs, Mode m) {
    stone = int(lbs) / Lbs_per_stn;
    pds_left = int(lbs) % Lbs_per_stn + lbs - int(lbs);
    pounds = lbs;
    mode = m;

}

Stonewt::Stonewt(int stn, double lbs, Mode m) {
    stone = stn;
    pds_left = lbs;
    pounds = stn * Lbs_per_stn + lbs;
    mode = m;
}

std::ostream &operator<<(std::ostream &os, const Stonewt & st) {
    if (st.mode == Stonewt::Stone) {
        os << st.stone << " stones, " << st.pds_left << " pounds\n";
    } else {
        os << st.pounds << " pounds\n";
    }
    return os;
}

Stonewt Stonewt::operator+(const Stonewt &s) const{
    Stonewt result;
    double p = pounds + s.pounds;
    result.pounds = p;
    result.stone = int(p) / Lbs_per_stn;
    result.pds_left = int(p) % Lbs_per_stn + p - int(p);
    return result;
}

Stonewt Stonewt::operator-(const Stonewt &s) const{
    Stonewt result;
    double p = pounds - s.pounds;
    result.pounds = p;
    result.stone = int(p) / Lbs_per_stn;
    result.pds_left = int(p) % Lbs_per_stn + p - int(p);
    return result;
}

Stonewt Stonewt::operator*(double m) const {
    Stonewt result;
    double p = pounds * m;
    result.pounds = p;
    result.stone = int(p) / Lbs_per_stn;
    result.pds_left = int(p) % Lbs_per_stn + p - int(p);
    return result;
}

Stonewt operator*(double m, const Stonewt & st) {
    return st * m;
}
Stonewt::Stonewt() {
    stone = pounds = pds_left = 0;
    mode = Stone;
}

Stonewt::~Stonewt() {}

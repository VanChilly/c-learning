// File Name: main.cc
// Created by VanChilly on 2022/9/25.
//
#include "iostream"
#include "stonewt.h"

#define ll long long
using namespace std;

int main() {
    Stonewt st[6] = {
            {2, 0},
            {11, 0},
            {12, 0}
    };
    for (int i = 3; i < 6; i ++ ) {
        int stn = 0;
        cout << "Enter No. "<< i + 1 << " stones: " << endl;
        cin >> stn;
        st[i] = Stonewt(stn, 0);
    }
    Stonewt mx = st[0], mn = st[0];
    int cnt = 0;
    Stonewt com_st = Stonewt(11, 0);
    for (auto item : st) {
        if (item > mx) {
            mx = item;
        } else if (item < mn) {
            mn = item;
        }
        if (item >= com_st) {
            cnt += 1;
        }
    }
    cout << "The biggest object is " << mx << endl;
    cout << "The smallest object is " << mn << endl;
    cout << cnt << " objects are bigger than " << com_st << endl;

}

// File Name: randwalk.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "cstdlib"
#include "ctime"
#include "vector.h"

#define ll long long
using namespace std;

int main() {
    using namespace std;
    using VECTOR::Vector;
    srand(time(0)); // seed random-number generator
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps = 0;
    double target;
    double dstep;
    cout << "Enter target distance (q to quit): ";
    double mx{}, mn = 1e9, sum{}, cnt{};
    while (cin >> target) {
        cout << "Enter step length: ";
        if (!(cin >> dstep))
            break;
        while (result.magval() < target) {
            direction = rand() % 360;
            step.reset(dstep, direction, Vector::POL);
            result = result + step;
            steps ++;
        }
        mx = mx > steps ? mx : steps;
        mn = mn < steps ? mn : steps;
        sum += steps;
        cnt += 1;
        cout << "After " << steps << " steps, the subject "
                                     "has the following location: \n";
        cout << cnt << " tests: \n" << "max: " << mx << endl << "min: " << mn << endl << "ave: " << sum * 1.0 / cnt << endl;
        cout << result << endl;
        result.polar_mode();
        cout << " or\n" << result << endl;
        cout << "Average outward distance per step = "
        << result.magval() / steps << endl;
        result.rect_mode();
        steps = 0;
        result.reset(0.0, 0.0);
        cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    cin.clear();
    while (cin.get() != '\n')
        continue;
    return 0;
}

//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"
#define ll long long
using namespace std;

int main() {
    ll worldPopulation, USPopulation;
    cout << "Enter the world's population: ";
    cin >> worldPopulation;
    cout << "Enter the population of the US: ";
    cin >> USPopulation;
    double proportion = double(USPopulation) / double(worldPopulation) * 100;
    cout << "The population of the US is " << proportion << '%' << " of the world population.";
}
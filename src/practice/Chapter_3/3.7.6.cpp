//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    ll miles, gallon;
    cout << "Enter miles: ";
    cin >> miles;
    cout << "Enter gallons: ";
    cin >> gallon;
    cout << "Your oil cost is " << double(miles) / double(gallon) << " miles / gallon.";
}

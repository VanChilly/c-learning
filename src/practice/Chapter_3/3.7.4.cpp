//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    ll seconds;
    const int hoursInDay = 24;
    const int minutesInHour = 60;
    const int secondInMinute = 60;
    cout << "Enter the number of seconds: ";
    cin >> seconds;
    ll init_seconds = seconds;
    int days, hours, minutes;
    days = seconds / (hoursInDay * minutesInHour * secondInMinute);
    seconds -= days * (hoursInDay * minutesInHour * secondInMinute);
    hours = seconds / (minutesInHour * secondInMinute);
    seconds -= hours * minutesInHour * secondInMinute;
    minutes = seconds / secondInMinute;
    seconds -= minutes * secondInMinute;
    cout << init_seconds << " seconds = " << days << " days, " << hours
            << " hours, " << minutes << " minutes, " << seconds << " seconds";
}

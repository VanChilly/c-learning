//
// Created by VanChilly on 2022/9/2.
//
#include <valarray>
#include "iostream"

using namespace std;

int main() {
    int ft, in;
    cout << "Please input your height:" << endl;
    cout << "Please input ft:" << ' ';
    cin >> ft;
    cout << "Please input in:" << ' ';
    cin >> in;
    cout << "Please input your weight:" << endl;
    double weight;
    cin >> weight;
    double height;
    const int ft2in_factor = 12;
    const double in2meter_factor = 0.0254;
    const double pound2kg_factor = 1 / 2.2;
    double BMI;
    height = (ft * ft2in_factor + in) * in2meter_factor;
    cout << "Your height is " << height << 'm' << endl;
    weight = pound2kg_factor * weight;
    cout << "Your weight is " << weight << "kg" << endl;
    BMI = weight / pow(height, 2);
    cout << "Your BMI is " << BMI << endl;

}
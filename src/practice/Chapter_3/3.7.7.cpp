//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    ll liter, milesPerGallon;
    cout << "Enter UK Style oil cost: ";
    cin >> liter;
    const double liter2gallon = 3.875;
    const double km2miles = 62.14;
    double gallon = double(liter) / double(liter2gallon);
    cout << "US Style: " << km2miles / gallon << " miles / gallon.";
}
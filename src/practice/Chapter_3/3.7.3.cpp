//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"

using namespace std;

int main() {
    int degrees, minutes, seconds;
    double ans;
    cout << "Enter a latitude in degrees, minutes, and seconds:" << endl;
    cout << "First, enter the degrees: " << endl;
    cin >> degrees;
    cout << "Next, enter the minutes of arc: " << endl;
    cin >> minutes;
    cout << "Finally, enter the seconds of arc: " << endl;
    cin >> seconds;
    ans = degrees + minutes / 60.0 + seconds / 3600.0;
    cout << degrees << " degrees," << minutes << " minutes, " << seconds << " seconds = " << ans << " degrees";
}

//
// Created by VanChilly on 2022/9/2.
//
#include "iostream"
using namespace std;

int main() {
    int height;
    cin >> height;
    const int ft2in_factor = 12;
    const double in2ft_factor = 1.0 / 12;
    cout << height * in2ft_factor;
}

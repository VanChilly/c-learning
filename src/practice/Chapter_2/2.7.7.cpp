//
// Created by VanChilly on 2022/9/1.
//
#include "iostream"
using namespace std;

void show(int hours, int minutes);
int main() {
    cout << "Enter the number of hours:  ";
    int hours, minutes;
    cin >> hours;
    cout << "Enter the number of minutes:  ";
    cin >> minutes;
    show(hours, minutes);
}

void show(int hours, int minutes) {
    cout << "Time: " << hours << ":" << minutes << endl;
}
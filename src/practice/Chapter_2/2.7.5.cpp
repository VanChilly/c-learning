//
// Created by VanChilly on 2022/9/1.
//
#include "iostream"

using namespace std;

double getFahTemp(double celTemp);

int main() {
    cout << "Please enter a Celsius value:" << endl;
    double celTemp, fahTemp;
    cin >> celTemp;
    fahTemp = getFahTemp(celTemp);
    cout << celTemp << " degrees Celsius is " << fahTemp << " degrees Fahrenheit." << endl;
}

double getFahTemp(double celTemp) {
    double fahTemp;
    fahTemp = (celTemp) * (9.0 / 5) + 32;
    return fahTemp;
}
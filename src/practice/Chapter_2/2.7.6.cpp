//
// Created by VanChilly on 2022/9/1.
//
#include "iostream"
using namespace std;

double transform(double lightYear);
int main() {
    double lightYear, units;
    cout << "Enter the number of light years:" << endl;
    cin >> lightYear;
    units = transform(lightYear);
    cout << lightYear << " light years = " << units << " astronomical units." << endl;
}

double transform(double lightYear) {
    double units;
    units = lightYear * 63240;
    return units;
}
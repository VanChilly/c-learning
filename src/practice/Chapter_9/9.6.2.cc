// File Name: 9.6.2.cc
// Created by VanChilly on 2022/9/15.
//
#include "iostream"

#define ll long long
using namespace std;

void strcount(string & s) {
    static int total;
    total += s.length();
    cout << "\"" << s << "\"" << " contains " << s.length()
    << " characters\n" << total << " characters total" << endl;

}
int main() {
    string s;
    cout << "Enter string: " << endl;
    getline(cin, s);
    while (s != "") {
        strcount(s);
        cout << "Enter next line (empty line to quit): " << endl;
        getline(cin, s);
    }
    cout << "Bye";

}

// File Name: sales.cc
// Created by VanChilly on 2022/9/15.
//
#include "iostream"
#include "sales.h"

#define ll long long
using namespace std;

void SALES::setSales(SALES::Sales & s, const double ar[], int n) {
    double sum, max, min;
    sum = max = min = ar[0];
    s.sales[0] = ar[0];
    for (int i = 1; i < n; i ++ ) {
        s.sales[i] = ar[i];
        sum += ar[i];
        if (ar[i] > max) {
            max = ar[i];
        } else min = ar[i];
    }
    s.average = sum / n;
    s.min = min;
    s.max = max;
}

void SALES::setSales(SALES::Sales &s) {
    double ar[SALES::QUARTERS];
    for (int i = 0; i < SALES::QUARTERS; i ++ ) {
        cout << "Enter No. " << i + 1 << " sales: ";
        cin >> ar[i];
    }
    SALES::setSales(s, ar, SALES::QUARTERS);
}

void SALES::showSales(const SALES::Sales &s) {
    cout << "Sales: " << endl;
    for (double sale : s.sales)
        cout << sale << " ";
    cout << endl;
    cout << "Average: " << s.average << endl
    << "Max: " << s.max << endl << "Min: " << s.min;
}
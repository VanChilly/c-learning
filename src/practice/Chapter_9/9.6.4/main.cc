// File Name: main.cc
// Created by VanChilly on 2022/9/16.
//
#include "iostream"
#include "sales.h"

#define ll long long
using namespace std;

int main() {
    SALES::Sales s;
    SALES::setSales(s);
    SALES::showSales(s);
}

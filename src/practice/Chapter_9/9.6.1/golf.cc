// File Name: golf.cc
// Created by VanChilly on 2022/9/15.
//
#include <cstring>
#include "iostream"
#include "golf.h"
#define ll long long

void setgolf(golf & g, const char * name, int hc) {
    strcpy(g.fullname, name);
    g.handicap = hc;
}

int setgolf(golf & g) {
    using namespace std;
    char name[Len];
    int hc;
    cout << "Enter name: " << endl;
    cin.getline(name, Len);
    if (!name[0]) {
        return 0;
    } else {
        strcpy(g.fullname, name);
        cout << "Enter hc: " << endl;
        cin >> hc;
        cin.get();
        handicap(g, hc);
        return 1;
    }
}

void handicap(golf & g, int hc) {
    g.handicap = hc;
}

void showgolf(const golf & g) {
    using namespace std;
    cout << "Name: " << g.fullname << endl;
    cout << "hc: " << g.handicap << endl;
}
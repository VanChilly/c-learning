// File Name: golf.h
// Created by VanChilly on 2022/9/15.
//

#ifndef SRC_GOLF_H
#define SRC_GOLF_H

const int Len = 40;
struct  golf {
    char fullname[Len];
    int handicap;
};

void setgolf(golf & g, const char * name, int hc);
int setgolf(golf & g);
void handicap(golf & g, int hc);
void showgolf(const golf & g);

#endif //SRC_GOLF_H

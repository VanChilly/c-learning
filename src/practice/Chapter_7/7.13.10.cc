// File Name: 7.13.10.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

double calculate(double, double, double (*pf)(double, double));
double add(double, double);
double mul(double, double);
double minus_(double, double);

int main() {
    double (*pf[3])(double, double) = {add, mul, minus_};
    for (auto item : pf) {
        double ans = calculate(1, 2, item);
        cout << ans << endl;
    }

}

double calculate(double a, double b, double (*pf)(double x, double y)) {
    return pf(a, b);
}

double add(double a, double b) {
    return a + b;
}

double mul(double a, double b) {
    return a * b;
}

double minus_(double a, double b) {
    return a - b;
}
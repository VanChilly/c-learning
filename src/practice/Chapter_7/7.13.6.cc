// File Name: 7.13.6.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

int Fill_array(double *, int);
void Show_array(const double *, int);
void Reverse_array(double *, int);

const int capacity = 10;
int main() {
    double arr[capacity];
    int cnt;
    cout << "Fill Array" << endl;
    cnt = Fill_array(arr, capacity);
    cout << "Show Array" << endl;
    Show_array(arr, cnt);
    cout << "Reverse Array" << endl;
    Reverse_array(arr, cnt);
    cout << "Show Array" << endl;
    Show_array(arr, cnt);
    cout << "Reverse Array except the first number and the last" << endl;
    Reverse_array(arr + 1, cnt - 2);
    cout << "Show Array" << endl;
    Show_array(arr, cnt);
}

int Fill_array(double * arr, int cap) {
    int cnt{}, num{};
    cout << "Enter No." << cnt + 1 << " number: ";
    while ((cin >> num) && (cnt < cap)) {
        arr[cnt++] = num;
        cout << "Enter No." << cnt + 1 << " number: ";
    }
    cout << "You actually enter " << cnt << " numbers.\n";
    return cnt;
}

void Show_array(const double * arr, int cnt) {
    for (int i = 0; i < cnt; i ++ ) {
        cout << "The No." << i + 1 << " number is " << arr[i] << endl;
    }
}

void Reverse_array(double * arr, int cnt) {
   for (int i = 0; i < cnt / 2; i ++ ) {
       auto tmp = arr[i];
       arr[i] = arr[cnt - i - 1];
       arr[cnt - i - 1] = tmp;
   }
}
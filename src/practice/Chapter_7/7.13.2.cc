// File Name: 7.13.2.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

const int num_grades = 10;

void GetGolfGrades(double *, int &);
void ShowAllGrades(const double *, int);
void GetMeanOfGolfGrades(const double *, int);
int main() {
    double golf_grades[num_grades];
    int cnt{};
    GetGolfGrades(golf_grades, cnt);
    ShowAllGrades(golf_grades, cnt);
    GetMeanOfGolfGrades(golf_grades, cnt);
}

void GetGolfGrades(double * golf_grades, int & cnt) {
    double input_grade;
    cout << "Enter NO. " << cnt + 1 << " grade (negative number to exit): ";
    cin >> input_grade;
    while ((input_grade >= 0) and (cnt < num_grades)) {
        golf_grades[cnt++] = input_grade;
        cout << "Enter NO. " << cnt + 1 << " grade (negative number to exit): ";
        cin >> input_grade;
    }
}

void ShowAllGrades(const double * golf_grades, int cnt) {
    for (int i = 0; i < cnt; i ++ ) {
        cout << "The golf grade of NO. " << i + 1 << " is " << golf_grades[i] << endl;
    }
}

void GetMeanOfGolfGrades(const double * golf_grades, int cnt) {
    double sum = 0;
    for (int i = 0; i < cnt; i ++ ) {
        sum += golf_grades[i];
    }
    cout << "The mean of " << cnt << " grades is " << sum / cnt << endl;
}
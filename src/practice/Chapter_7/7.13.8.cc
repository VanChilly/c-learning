// File Name: 7.13.8.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

const int Seasons = 4;
const char * Snames[Seasons] = {"Spring", "Summer", "Fall", "Winter"};
void fill(double * expenses);
void show(const double * expenses);

int main() {
//    double expenses[Seasons]; // version 1
    struct Expense {
        double expenses[Seasons];
    };  // version 2
    Expense expense;
    fill(expense.expenses);
    show(expense.expenses);
}

void fill(double * expenses) {
    for ( int i = 0; i < Seasons; i ++ ) {
        cout << "Enter " << Snames[i] << " expenses: ";
        cin >> expenses[i];
    }
}

void show(const double * expenses) {
    double total = 0.0;
    cout << "\nEXPENSES\n";
    for (int i = 0; i < Seasons; i ++ ) {
        cout << Snames[i] << ": $" << expenses[i] << endl;
        total += expenses[i];
    }
    cout << "Total Expenses: $" << total << endl;
}

// File Name: 7.13.9.cc
// Created by VanChilly on 2022/9/11.
//
#include <cstring>
#include "iostream"

#define ll long long
using namespace std;

const int SLEN = 30;
struct student {
    char fullname[SLEN];
    char hobby[SLEN];
    int ooplevel;
};
int getinfo(student pa[], int n);
void display1(student st);
void display2(const student * ps);
void display3(const student pa[], int n);

int main() {
    cout << "Enter class size: ";
    int class_size;
    cin >> class_size;
    while (cin.get() != '\n') continue;
    student * ptr_stu = new student[class_size];
    int entered = getinfo(ptr_stu, class_size);
    for ( int i = 0; i < entered; i ++ ) {
        display1(ptr_stu[i]);
        display2(&ptr_stu[i]);
    }
    display3(ptr_stu, entered);
    delete [] ptr_stu;
    cout << "Done\n";
    return 0;
}

int getinfo(student pa[], int n) {
    char name[SLEN], hobby[SLEN];
    int ooplevel;
    int i = 0;
    for (; i < n; i ++ ) {
        cout << "Enter name: " << endl;
        if (cin.getline(name, SLEN)) {
            strcpy(pa[i].fullname, name);
        }
        cout  << "Enter hobby: " << endl;
        if (cin.getline(hobby, SLEN)) {
            strcpy(pa[i].hobby, hobby);
        }
        cout << "Enter ooplevel: " << endl;
        if (cin >> ooplevel) {
            pa[i].ooplevel = ooplevel;
        }
        cin.get();
    }
    return i;
}

void display1(student st) {
    cout << "Student name: " << st.fullname << endl;
    cout << "Student hobby: " << st.hobby << endl;
    cout << "Student ooplevel: " << st.ooplevel << endl;
}

void display2(const student * ps) {
    cout << "Student name: " << ps->fullname << endl;
    cout << "Student hobby: " << ps->hobby << endl;
    cout << "Student ooplevel: " << ps->ooplevel << endl;
}

void display3(const student pa[], int n) {
    for (int i = 0; i < n; i ++ ) {
        cout << "No." << i + 1 << " student: " << endl;
        cout << "Student name: " << pa[i].fullname << endl;
        cout << "Student hobby: " << pa[i].hobby << endl;
        cout << "Student ooplevel: " << pa[i].ooplevel << endl;
    }
}
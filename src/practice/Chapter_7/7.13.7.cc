// File Name: 7.13.7.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

const int Max = 5;
double * fill_array(double * ar, int limit);
void show_array(const double * start, const double * end);
void revalue(double r, double * ar, const double * end);

int main() {
   double properties[Max];
   double * end = fill_array(properties, Max);
   show_array(properties, end);
   if (end - properties > 0) {
       cout << "Enter revaluation factor: ";
       double factor;
       while (!(cin >> factor)) {
           cin.clear();
           while (cin.get() != '\n') continue;
           cout << "Bad input; Please enter a number: ";
       }
       revalue(factor, properties, end);
       show_array(properties, end);
   }
   cout << "Done.\n";
   cin.get();
   cin.get();
   return 0;
}

double * fill_array(double * ar , int limit) {
    double temp;
    int i;
    for (i = 0; i < limit; i ++ ) {
        cout << "Enter value #" << (i + 1) << ": ";
        cin >> temp;
        if (!cin) {
            cin.clear();
            while (cin.get() != '\n') continue;
            cout << "Bad input; input process terminated.\n";
            break;
        } else if (temp < 0) break;
        ar[i] = temp;
    }
    return ar + i;
}

void show_array(const double * start, const double * end) {
    for (int i = 0; i < end - start; i ++ ) {
        cout << "Property #" << (i + 1) << ": $";
        cout << start[i] << endl;
    }
}

void revalue(double r, double * ar, const double * end) {
    for (int i = 0; i < end - ar; i ++) ar[i] *= r;
}
// File Name: 7.13.1.cpp
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

double GetHarmonicMean(double a, double b);

int main() {
    ll a{}, b{};
    cout << "Enter two numbers to get their harmonic mean: ";
    cin >> a >> b;
    while (a != 0 and b != 0) {
        double harmonic_mean = GetHarmonicMean(a, b);
        cout << "The harmonic mean of number " << a << " and number " << b
        << " is " << harmonic_mean << endl;
        cout << "Enter two numbers to get their harmonic mean: ";
        cin >> a >> b;
    }
}

double GetHarmonicMean(double a, double b) {
    return 2.0 * a * b / (a + b);
}

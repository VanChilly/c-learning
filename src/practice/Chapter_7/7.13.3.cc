// File Name: 7.13.3.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

struct box {
    char maker[40];
    float height;
    float width;
    float length;
    float volume;
};

void ShowMemberValue(box);
void SetVolume(box *);

int main() {
    box new_box = {"VanChilly", 1, 2, 3, 0};
    ShowMemberValue(new_box);
    SetVolume(&new_box);
    ShowMemberValue(new_box);
}

void ShowMemberValue(box new_box) {
    cout << "The maker is " << new_box.maker << endl;
    cout << "The height is " << new_box.height << endl;
    cout << "The width is " << new_box.width << endl;
    cout << "The length is " << new_box.length << endl;
    cout << "The volume is " << new_box.volume << endl;
}

void SetVolume(box * new_box) {
    new_box->volume = new_box->height * new_box->width * new_box->length;
}

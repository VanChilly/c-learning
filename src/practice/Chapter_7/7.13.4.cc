// File Name: 7.13.4.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

long double probability(unsigned, unsigned, unsigned, unsigned);

int main() {
    unsigned total, choices;
    unsigned s_total, s_choices;
    cout << "Enter the total number of choices on the game card and\n"
            "the number of picks allowed, the total number of special choices\n"
            "and the number of special picks allowed: \n";
    while ((cin >> total >> choices >> s_total >> s_choices) && choices <= total
            && s_choices <= s_total) {
        cout << "You have one chance in ";
        cout << probability(total, choices, s_total, s_choices);
        cout << " of winning.\n";
        cout << "Next four numbers (q to quit): ";
    }
    cout << "bye\n";
    return 0;
}

long double probability(unsigned numbers, unsigned picks, unsigned s_total, unsigned s_choices) {
    long double result = 1.0;
    long double n;
    unsigned p;

    for (n = numbers, p = picks; p > 0; n --, p --)
        result = result * n / p;
    return 1 / (result * (s_total / 1.0 * s_choices));
}

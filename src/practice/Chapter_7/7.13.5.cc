// File Name: 7.13.5.cc
// Created by VanChilly on 2022/9/11.
//
#include "iostream"

#define ll long long
using namespace std;

int ReportFactorial(int);

int main() {
    int num;
    cout << "Enter a number to get its factorial: ";
    int ans;
    while (cin >> num) {
        ans = ReportFactorial(num);
        cout << "The factorial of " << num << " is " << ans << endl;
        cout << "Enter a number to get its factorial: ";
    }
}

int ReportFactorial(int num) {
    if (num <= 0) return 1;
    return num * ReportFactorial(num - 1);
}

//
// Created by VanChilly on 2022/9/4.
//
#include <array>
#include "iostream"

#define ll long long
using namespace std;

const int Arsize = 101;
int main() {
    array<long double, Arsize> a{};
    a[0] = a[1] = 1LL;
    for (int i = 2; i <= Arsize - 1; i++) {
        a[i] = a[i - 1] * i;
    }
    cout << "100! = " << a.back();

}
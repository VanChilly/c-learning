//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

struct Car {
    string make;
    int year{};
};
int main() {
    int num{};
    cout << "How many cars do you wish to catalog? ";
    cin >> num;
    Car* cars = new Car[num];
    int i = -1;
    while (i ++ < num - 1) {
        cout << "Car #" << i + 1 << ":" << endl;
        cout << "Please enter the make: ";
        cin.get();
        getline(cin, cars[i].make);
        cout << "Please enter the year made: ";
        cin >> cars[i].year;
    }

    i = -1;
    cout << "Here is your collection:" << endl;
    while (i ++ < num - 1) {
        cout << cars[i].year << " " << cars[i].make << endl;
    }





}

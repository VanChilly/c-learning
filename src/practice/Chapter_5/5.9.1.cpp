//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    int l, r;
    cout << "Enter smallest number: ";
    cin >> l;
    cout << "Enter biggest number: ";
    cin >> r;
    int ans = 0;
    for (int i = l; i <= r; i ++ ) {
        ans += i;
    }
    cout << "Sum between " << l << " and " << r << " is " << ans;
}

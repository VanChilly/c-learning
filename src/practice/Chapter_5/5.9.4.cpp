//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

const double duplicate = 0.05;
int main() {
    int Daphnc = 100;
    double Cleo = 100;
    const int single = Daphnc * 0.1;
    int year = 0;
    while (Cleo <= Daphnc) {
        Daphnc += single;
        Cleo += Cleo * duplicate;
        year += 1;
    }
    cout << "Passed " << year << " years, Cleo has "
    << Cleo << " dollars and Daphc has " << Daphnc << " dollars.";
}

//
// Created by VanChilly on 2022/9/5.
//
#include "iostream"
#include "cstring"

#define ll long long
using namespace std;

const int MAX = 100;
int main() {
    char cc[MAX];
    cout << "Enter words (to stop, type the word done):" << endl;
    cin >> cc;
    int cnt = 0;
    while (strcmp(cc, "done") != 0) {
        cin >> cc;
        cnt += 1;
    }
    cout << "You entered a total of " << cnt << " words.";
}

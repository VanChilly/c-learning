//
// Created by VanChilly on 2022/9/4.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    ll s{};
    ll in;
    cout << "Enter a number: ";
    cin >> in;
    while (in != 0) {
        cout << "Enter a number: ";
        s += in;
        cin >> in;
    }
    cout << "The accumulated sum is " << s;

}
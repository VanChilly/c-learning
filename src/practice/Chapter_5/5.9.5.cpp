//
// Created by VanChilly on 2022/9/4.
//
#include <vector>
#include "iostream"

#define ll long long
using namespace std;

int main() {
    string s[12]{"Jan", "Feb", "Mar", "Apr", "May",
                 "Jun", "Jul", "Aug", "Sep", "Oct",
                 "Nov", "Dec"};
    vector<int> arr(12);
    ll sum{};
    for(int i = 0; i < 12; i ++) {
        cout << "Enter the " << s[i] << "'s sales: ";
        cin >> arr[i];
        sum += arr[i];
        cout << s[i] << "'s sales " << arr[i];
        cout << endl;
    }
    cout << "Full year sold " << sum;

}

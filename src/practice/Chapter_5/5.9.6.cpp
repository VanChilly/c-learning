//
// Created by VanChilly on 2022/9/4.
//
#include <vector>
#include "iostream"

#define ll long long
using namespace std;

int main() {
    vector<vector<int>> arr(3, vector<int>(12));
    int all{};
    for (int i = 0; i < arr.size(); i ++ ) {
        int sum{};
        for (int j = 0; j < arr[0].size(); j ++ ) {
            cin >> arr[i][j];
            sum += arr[i][j];
        }
        all += sum;
        cout << "No." << i + 1 << " year sold " << sum << endl;
    }
    cout << arr.size() << "years sold " << all << " totally.";
}

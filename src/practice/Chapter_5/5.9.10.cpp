//
// Created by VanChilly on 2022/9/5.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    int num;
    cout << "Enter number of rows: ";
    cin >> num;
    int i = -1;
    while (i ++ < num - 1) {
        for(int j = 0; j < num - i - 1; j ++ ) {
            cout << ". ";
        }
        for(int j = 0; j < i + 1; j ++) {
            cout << "* ";
        }
        cout << endl;
    }
}

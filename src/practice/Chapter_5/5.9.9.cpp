//
// Created by VanChilly on 2022/9/5.
//

#include "iostream"

#define ll long long
using namespace std;

int main() {
    string s;
    cout << "Enter words (to stop, type the word done):" << endl;
    cin >> s;
    int cnt = 0;
    while (s != "done") {
        cin >> s;
        cnt += 1;
    }
    cout << "You entered a total of " << cnt << " words.";
}
// File Name: account.h
// Created by VanChilly on 2022/9/17.
//
#ifndef SRC_ACCOUNT_H
#define SRC_ACCOUNT_H
#include "string"

using namespace std;
class Account {
private:
    string name_;
    string account_;
    double deposit_;
public:
    Account(string name="anonymouse", string account="test", double deposit=0.0);
    void Show();
    void Deposit(double deposit);
    void TakeOut(double deposit);
};

#endif //SRC_ACCOUNT_H

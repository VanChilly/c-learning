// File Name: account.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "account.h"
#define ll long long
using namespace std;

Account::Account(std::string name, std::string account, double deposit) {
    name_ = name;
    account_ = account;
    deposit_ = deposit;
}
void Account::Show() {
    cout << "Name: " << name_ << endl;
    cout << "Account: " << account_ << endl;
    cout << "Deposit: " << deposit_ << endl;
}

void Account::Deposit(double deposit) {
    deposit_ += deposit;
}

void Account::TakeOut(double deposit) {
    if (deposit_ > deposit) {
        deposit_ -= deposit;
    }
    else cout << "Not enough deposit, rest: " << deposit_ << endl;
}

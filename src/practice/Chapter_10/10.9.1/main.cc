// File Name: main.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "account.h"

#define ll long long
using namespace std;

int main() {
    Account acc = Account("VC");
    acc.Show();
    acc.Deposit(10);
    acc.Show();
    acc.TakeOut(5);
    acc.Show();
    acc.TakeOut(10);
    acc.Show();
    return 0;
}

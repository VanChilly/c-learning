// File Name: person.h
// Created by VanChilly on 2022/9/17.
//
#ifndef SRC_PERSON_H
#define SRC_PERSON_H
#include "iostream"

using namespace std;

class Person {
private:
    static const int LIMIT = 25;
    string lname;
    char fname[LIMIT];
public:
    Person() {lname = ""; fname[0] = '\0';}
    Person(const string & ln, const char * fn = "Heyyou");
    void Show() const;
    void FormalShow() const;
};

#endif //SRC_PERSON_H

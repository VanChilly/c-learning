// File Name: main.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "person.h"

#define ll long long
using namespace std;

int main() {
    Person one;
    Person two("Smythecraft");
    Person three("Dimwiddy", "Sam");
    one.Show();
    one.FormalShow();
    cout << endl;
    two.Show();
    two.FormalShow();
    cout << endl;
    three.Show();
    three.FormalShow();
}

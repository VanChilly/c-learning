// File Name: person.cc
// Created by VanChilly on 2022/9/17.
//
#include <cstring>
#include "iostream"
#include "person.h"

using namespace std;

Person::Person(const std::string &ln, const char *fn) {
    lname = ln;
    strcpy(fname, fn);
}

void Person::Show() const {
    cout << fname << " " << lname << endl;
}

void Person::FormalShow() const {
    cout << lname << " " << fname << endl;
}

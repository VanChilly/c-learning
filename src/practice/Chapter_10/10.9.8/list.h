// File Name: list.h
// Created by VanChilly on 2022/9/18.
//

#ifndef SRC_LIST_H
#define SRC_LIST_H

struct array {
    double val;
};
typedef array Item;
class List {
private:
    static const int MAX = 5;
    Item items[MAX];
    int top;
public:
    List();
    List(Item * items_, int n);
    bool add(Item & item);
    bool empty();
    bool full();
    void visit(void (*pf)(Item &));
};
#endif //SRC_LIST_H

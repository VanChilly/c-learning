// File Name: list.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "list.h"

#define ll long long
using namespace std;

List::List() {
    top = 0;
}

List::List(Item *items_, int n) {
    top = 0;
    if (n <= MAX) {
        int i = 0;
        for (;i < n; i ++ ) {
            items[i] = items_[i];
            top ++;
        }
    }
}

bool List::add(Item &item) {
    if (top == MAX) return false;
    items[top++] = item;
    return true;
}

bool List::empty() {
    return top == 0;
}

bool List::full() {
    return top == MAX;
}

void List::visit(void (*pf)(Item &)) {
    for(int i = 0; i < top; i ++) {
        pf(items[i]);
    }
}
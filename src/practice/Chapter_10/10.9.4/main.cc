// File Name: main.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "sales.h"

#define ll long long
using namespace std;
using namespace SALES;

int main() {
    Sales sales = Sales().setSales();
    sales.showSales();
}

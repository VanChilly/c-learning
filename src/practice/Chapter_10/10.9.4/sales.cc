// File Name: sales.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "sales.h"

#define ll long long
using namespace std;

SALES::Sales SALES::Sales::setSales() {
    double ar[QUARTERS];
    for (int i = 0; i < QUARTERS; i ++ ) {
        cout << "Enter No. " << i + 1 << " sales: ";
        cin >> ar[i];
    }
    *this = SALES::Sales(ar, QUARTERS);
    return *this;
}

void SALES::Sales::showSales() {
    cout << "Sales: " << endl;
    for (double sale : sales)
        cout << sale << " ";
    cout << endl;
    cout << "Average: " << average << endl
         << "Max: " << max << endl << "Min: " << min;
}
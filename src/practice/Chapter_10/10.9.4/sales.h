// File Name: sales.h
// Created by VanChilly on 2022/9/17.
//

#ifndef SRC_SALES_H
#define SRC_SALES_H
namespace SALES {
    class Sales {
    private:
        static const int QUARTERS = 4;
        double sales[QUARTERS];
        double average;
        double max;
        double min;
    public:
        Sales() {
            double tmp[4] = {1, 2, 3, 4};
            Sales(tmp, 4);
        };
        Sales(double ar[], const int n) {
            double sum, max_, min_;
            sum = max_ = min_ = ar[0];
            sales[0] = ar[0];
            for (int i = 1; i < n; i ++ ) {
                sales[i] = ar[i];
                sum += ar[i];
                if (ar[i] > max_) {
                    max_ = ar[i];
                } else min_ = ar[i];
            }
            average = sum / n;
            min = min_;
            max = max_;

        };
        Sales setSales();
        void showSales();
    };
}
#endif //SRC_SALES_H

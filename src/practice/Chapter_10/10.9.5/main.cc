// File Name: main.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "stack.h"

#define ll long long
using namespace std;

int main() {
    Stack s;
    double sum = 0;
    customer c[3] = {
            {"c1", 20},
            {"c2", 30},
            {"c3", 40}
    };
    for (auto item : c) {
        s.push(item);
    }
    while (!s.isempty()) {
        customer tmp;
        s.pop(tmp);
        sum += tmp.payment;
    }
    cout << "Sum: " << sum;
}

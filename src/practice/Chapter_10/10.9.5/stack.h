// File Name: stack.h
// Created by VanChilly on 2022/9/18.
//

#ifndef SRC_STACK_H
#define SRC_STACK_H

struct customer {
    char fullname[35];
    double payment;
};
typedef customer Item;

class Stack {
private:
    enum {MAX = 10};
    Item items[MAX];
    int top;
public:
    Stack();
    bool isempty() const;
    bool isfull() const;
    bool push(const Item & item);
    bool pop(Item & item);
};


#endif //SRC_STACK_H

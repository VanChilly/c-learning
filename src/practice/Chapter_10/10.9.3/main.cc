// File Name: main.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "golf.h"

#define ll long long
using namespace std;

int main() {
    Golf g1;
    Golf g2 = Golf("hh");
    Golf g3 = Golf().setGolf();
    g1.showGolf();
    g2.showGolf();
    g3.showGolf();
}

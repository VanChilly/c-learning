// File Name: golf.h
// Created by VanChilly on 2022/9/17.
//

#ifndef SRC_GOLF_H
#define SRC_GOLF_H

#include <cstring>

class Golf {
private:
    static const int Len = 40;
    char fullname[Len];
    int handicap;
public:
    Golf(const char * fname = "vc", int hc = 0) {
        strcpy(fullname, fname);
        handicap = hc;
    }
    Golf setGolf();
    void setHandicap(int hc);
    void showGolf() const;
};

#endif //SRC_GOLF_H

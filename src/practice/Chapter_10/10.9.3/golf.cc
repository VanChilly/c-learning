// File Name: golf.cc
// Created by VanChilly on 2022/9/17.
//
#include "iostream"
#include "golf.h"

using namespace std;

Golf Golf::setGolf() {
    char name[Len];
    int hc;
    cout << "Enter name: " << endl;
    cin.getline(name, Len);
    if (!name[0]) {
        return *this;
    } else {
        cout << "Enter hc: " << endl;
        cin >> hc;
        cin.get();
        *this = Golf(name, hc);
        return *this;
    }
}

void Golf::setHandicap(int hc) {
    handicap = hc;
}

void Golf::showGolf() const {
    cout << "Name: " << fullname << endl;
    cout << "Handicap: " << handicap << endl;
}
// File Name: move.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "move.h"

#define ll long long
using namespace std;

Move::Move(double a, double b) {
    x = a;
    y = b;
}

void Move::showmove() const {
    cout << "x: " << x << endl;
    cout << "y: " << y << endl;
}

Move Move::add(const Move &m) const {
    Move tmp;
    tmp.x = m.x + this->x;
    tmp.y = m.y + this->y;
    return tmp;
}

void Move::reset(double a, double b) {
    x = a;
    y = b;
}

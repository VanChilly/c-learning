// File Name: main.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "move.h"

#define ll long long
using namespace std;

int main() {
    Move a = Move(1, 1);
    a.showmove();
    Move b = Move(2, 2);
    b.showmove();
    Move c = a.add(b);
    c.showmove();
}

// File Name: main.cc
// Created by VanChilly on 2022/9/18.
//
#include "iostream"
#include "plorg.h"

#define ll long long
using namespace std;

int main() {
   Plorg p1;
   p1.showPlorg();
   Plorg p2 = Plorg("hc", 2);
   p2.showPlorg();
   p2.setCI(3);
   p2.showPlorg();

}

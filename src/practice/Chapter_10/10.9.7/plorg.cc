// File Name: plorg.cc
// Created by VanChilly on 2022/9/18.
//
#include <cstring>
#include "iostream"
#include "plorg.h"

#define ll long long
using namespace std;

Plorg::Plorg() {
    strcpy(name, "Plorga");
    CI = 50;
}

Plorg::Plorg(const char *name_, int CI_ = 50) {
    strcpy(name, name_);
    CI = CI_;
}

void Plorg::setCI(int CI_) {
    CI = CI_;
}

void Plorg::showPlorg() const {
    cout << "name: " << name << endl;
    cout << "CI: " << CI << endl;
}


// File Name: plorg.h
// Created by VanChilly on 2022/9/18.
//

#ifndef SRC_PLORG_H
#define SRC_PLORG_H

class Plorg {
private:
    static const int MAX = 19;
    char name[MAX];
    int CI;
public:
    Plorg();
    Plorg(const char * name_, int CI_);
    void setCI(int CI_);
    void showPlorg() const;
};

#endif //SRC_PLORG_H

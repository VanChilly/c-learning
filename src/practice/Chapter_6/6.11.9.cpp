// File Name: 6.11.9.cpp
// Created by VanChilly on 2022/9/9.
//
#include <fstream>
#include "iostream"

#define ll long long
using namespace std;

struct Contributor {
    string name;
    double payment;
};
const int thresh = 10000;
int main() {
    ifstream outFile;
    string filename = "G:\\MyCode\\c-learning\\src\\practice\\Chapter_6\\6.11.9.txt";
    outFile.open(filename);
    if (!outFile.is_open()) {
        cout << "Could not open file " << filename << "\n";
        exit(EXIT_FAILURE);
    }
    int num;
    int payment;
    string contri_name;
    outFile >> num;
    Contributor ctb[num];
    outFile.get();
    for (int i = 0; i < num; i ++ ) {
        getline(outFile, contri_name);
        cout << contri_name << "\n";
        ctb[i].name = contri_name;
        outFile >> payment;
        outFile.get();
        ctb[i].payment = payment;
    }
    if (outFile.eof()) cout << "End of the file reached.\n";
    else if (outFile.fail()) cout << "Input terminated by mismatch character.\n";
    else cout << "Input terminated by Unknow reason.\n";
    outFile.close();
    cout << "Grand Patrons: " << endl;
    int flag = 1;
    for (const auto& item : ctb) {
        if (item.payment > 10000)
        {
            flag = 0;
            cout << item.name << " " << item.payment << endl;
        }
    }
    if (flag) cout << "none" << endl;
    flag = 1;
    cout << "Patrons: " << endl;
    for (const auto& item : ctb) {
        if (item.payment <= 10000)
        {
            flag = 0;
            cout << item.name << " " << item.payment << endl;
        }
    }
    if (flag) cout << "none";
}

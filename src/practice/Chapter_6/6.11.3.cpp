// File Name: 6.11.3.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    enum {
        c = 'c', p = 'p', t = 't', g = 'g'
    };
    char ch;
    cout << "Please enter one of the following choices:" << endl
    << "c) carnivore\t" << "p) pianist" << endl
    << "t) tree\t\t\t" << "g) game" << endl;
    cin >> ch;
    while (1) {
        string s = "A maple is a ";
        int flag = 0;
        switch (ch) {
            case c: cout << s + "carnivore";
            break;
            case p: cout << s + "pianist";
            break;
            case t: cout << s + "tree";
            break;
            case g: cout << s + "game";
            break;
            default: flag = 1;
                break;
        }
        if (!flag) break;
        cout << "Please enter a c, p, t, or g:";
        cin >> ch;
    }
}
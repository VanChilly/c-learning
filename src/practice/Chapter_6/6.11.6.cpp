// File Name: 6.11.6.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;

struct Contributor {
    string name;
    double payment;
};
const int thresh = 10000;
int main() {
    Contributor ctb[2] = {
            {"Optimus", 10001},
            {"Deception", 100001}
    };
    cout << "Grand Patrons: " << endl;
    int flag = 1;
    for (const auto& item : ctb) {
        if (item.payment > 10000)
        {
            flag = 0;
            cout << item.name << " " << item.payment << endl;
        }
    }
    if (flag) cout << "none";
    flag = 1;
    cout << "Patrons: " << endl;
    for (const auto& item : ctb) {
        if (item.payment <= 10000)
        {
            flag = 0;
            cout << item.name << " " << item.payment << endl;
        }
    }
    if (flag) cout << "none";
}

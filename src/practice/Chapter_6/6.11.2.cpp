// File Name: 6.11.2.cpp
// Created by VanChilly on 2022/9/7.
//
#include <array>
#include "iostream"

#define ll long long
using namespace std;

int main() {
    array<double, 10> arr{};
    int i = 0;
    int l = 0;
    while (i ++ < 9) {
        char c;
        cin >> c;
        if (isdigit(c))
        {
            arr[i] = double(c - '0');
            l += 1;
        }
        else break;
    }
    int cnt = 0;
    double s_arr{};
    for(auto j : arr) {
        s_arr += j;
    }
    double mean = s_arr / l;
    for(auto j : arr) {
        if (j > mean) cnt += 1;
    }
    cout << "Mean value: " << mean << endl
    << "Has " << cnt << " numbers are bigger than than mean val " << mean;

}

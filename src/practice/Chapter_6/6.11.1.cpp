// File Name: 6.11.1.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    char ch;
    cin >> ch;
    string s;
    while(ch != '@') {
        if (isalpha(ch)) {
            if (isupper(ch)) ch = tolower(ch);
            else ch = toupper(ch);
            s += ch;
        }
        cin >> ch;
    }
    cout << s;
}

// File Name: 6.11.4.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;
// Benevolent Order of Programmers name structure
const int strsize = 20;
struct bop {
    char fullname[strsize]; // real name
    char title[strsize];     // job title;
    char bopname[strsize];  // secret BOP name
    int preference;         // 0 = fullname, 1 = title, 2 = bopname
};
int main() {
    bop arr[5] = {
            {"Wimp Macho", "WM", "wm", 0},
            {"Raki Rhodes", "RR", "Junior Programmer", 2},
            {"Celia Laiter", "CL", "MIPS", 2},
            {"Hoppy Hipman", "HH", "Analyst Trainee", 2},
            {"Pat Hand", "PH", "LOOPY", 2}
    };
    char ch;
    cout << "a. display by name\t" << "b. display by title" << endl
    << "c. display by bopname\t" << "d. display by preference" << endl
    << "q. quit" << endl;
    cout << "Enter your choice: ";
    cin >> ch;
    while (ch != 'q')  {
        for (auto item : arr) {
            if (ch == 'a') {
                cout << item.fullname << endl;
            } else if (ch == 'b') {
                cout << item.title << endl;
            } else if (ch == 'c') {
                cout << item.bopname << endl;
            } else {
                if (item.preference == 0)
                    cout << item.fullname << endl;
                else if (item.preference == 1)
                    cout << item.title << endl;
                else cout << item.bopname << endl;
            }
        }
        cout << "Enter your choice: ";
        cin >> ch;
    }
    cout << "Bye!";
}
// File Name: 6.11.7.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;

const char vowels[10] = {'a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U'};
int main() {
    cout << "Enter words (q to quit): " << endl;
    string s;
    char ch;
    cin >> s;
    ch = s[0];
    int cnt_vowels{}, consonants{}, others{};
    while (s != "q") {
        if (isalpha(ch)) {
            int flag = 1;
            for(auto item : vowels) {
                if (ch == item) {
                    cnt_vowels += 1;
                    flag = 0;
                    break;
                }
            }
            if (flag) consonants += 1;
        } else others += 1;
        cin >> s;
        ch = s[0];
    }
    cout << cnt_vowels << " words beginning with vowels" << endl
    << consonants << " words beginning with consonants" << endl
    << others << " others";
}

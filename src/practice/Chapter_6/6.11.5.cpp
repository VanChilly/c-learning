// File Name: 6.11.5.cpp
// Created by VanChilly on 2022/9/7.
//
#include "iostream"

#define ll long long
using namespace std;

int main() {
    const int arr[3] = {5000, 15000, 35000};
    const double factor[4] = {0, 0.1, 0.15, 0.2};
    double income;
    cout << "Enter your income: ";
    cin >> income;
    while (income > 0) {
        double tax{};
        for(int i = 2; i > -1; i -- ) {
            if (income > arr[i]) {
                tax += (income - arr[i]) * factor[i + 1];
                income = arr[i];
            }
        }
        cout << "Tax is " << tax << endl;
        cout << "Enter your income: ";
        cin >> income;
    }
}
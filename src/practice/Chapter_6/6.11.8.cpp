// File Name: 6.11.8.cpp
// Created by VanChilly on 2022/9/7.
//
#include <fstream>
#include "iostream"

#define ll long long
using namespace std;

int main() {
    ifstream fout;
    string filename = "G:\\MyCode\\c-learning\\src\\practice\\Chapter_6\\6.11.8.txt";
    fout.open(filename);
    if (!fout.is_open()) {
        cout << "Could not open the file " << filename << "\n";
        exit(EXIT_FAILURE);
    }
    string line;
    int cnt{};
    while (getline(fout, line)) {
        cnt += line.size();
    }
    if (fout.eof()) cout << "End of file reached.\n";
    else if (fout.fail()) cout << "Input terminated by data mismatch.\n";
    else cout << "Input terminated for unknown reason.\n";
    fout.close();
    cout << "The file 6.11.8.txt contains " << cnt << " characters.";
}
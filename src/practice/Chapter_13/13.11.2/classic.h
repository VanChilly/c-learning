// File Name: classic.h
// Created by VanChilly on 2022/9/25.
//

#ifndef SRC_CLASSIC_H
#define SRC_CLASSIC_H
#include "cd.h"

class Classic: public Cd {
private:
    char * work;
public:
    Classic(char * s1, char * s2, char * s3, int n, double x);
    Classic();
    void Report() const override;
    Classic & operator=(const Classic & c);
    ~Classic() override;
};
#endif //SRC_CLASSIC_H

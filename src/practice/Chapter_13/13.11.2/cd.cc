// File Name: cd.cc
// Created by VanChilly on 2022/9/25.
//
#include <cstring>
#include <iostream>
#include "cd.h"

Cd::Cd(char *s1, char *s2, int n, double x) {
    performers = new char[strlen(s1) + 1];
    std::strcpy(performers, s1);
    label = new char[strlen(s2) + 1];
    std::strcpy(label, s2);
    selections = n;
    playtime = x;
}

Cd::Cd(const Cd &d) {
    *this = d;
}

Cd::Cd() {
    performers = nullptr;
    label = nullptr;
    selections = 0;
    playtime = 0;
}

void Cd::Report() const {
    std::cout << "Performers: " << performers << '\n'
    << "Label: " << label << '\n'
    << "Selections: " << selections << '\n'
    << "Playtime: " << playtime << '\n';
}

Cd & Cd::operator=(const Cd &d) {
    if (this == &d) return *this;
    delete [] performers;
    performers = new char[std::strlen(d.performers) + 1];
    std::strcpy(performers, d.performers);
    delete [] label;
    label = new char[std::strlen(d.label) + 1];
    std::strcpy(label, d.label);
    selections = d.selections;
    playtime = d.playtime;
    return *this;
}

Cd::~Cd() {
    delete [] performers;
    delete [] label;
}


// File Name: classic.cc
// Created by VanChilly on 2022/9/25.
//
#include <cstring>
#include "iostream"
#include "classic.h"

Classic::Classic(char *s1, char *s2, char *s3, int n, double x):
    Cd(s1, s2, n, x) {
    work = new char[std::strlen(s3) + 1];
    std::strcpy(work, s3);
}

Classic::Classic(): Cd() {
    work = nullptr;
}
void Classic::Report() const {
    Cd::Report();
    std::cout << "Work: " << work << '\n';
}

Classic & Classic::operator=(const Classic &c) {
    if (this == &c) return *this;
    Cd::operator=(c);
    work = new char[std::strlen(c.work) + 1];
    std::strcpy(work, c.work);
    return *this;
}

Classic::~Classic() {
    delete [] work;
}
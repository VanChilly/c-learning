// File Name: base_dma.h
// Created by VanChilly on 2022/9/26.
//

#ifndef SRC_BASE_DMA_H
#define SRC_BASE_DMA_H

#include <iostream>
#include "ABC.h"

class baseDMA :public ABC{
public:
    baseDMA(char * l, int r);
    baseDMA(const baseDMA & rs);
    virtual ~baseDMA();
    virtual void View();
};


#endif //SRC_BASE_DMA_H

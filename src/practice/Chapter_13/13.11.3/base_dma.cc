// File Name: base_dma.cc
// Created by VanChilly on 2022/9/26.
//

#include "base_dma.h"
#include "cstring"

baseDMA::baseDMA(char *l, int r) : ABC(l, r){}

void baseDMA::View() {
    std::cout << "In baseDMA:" << std::endl;

    std::cout << *this << std::endl;
}

baseDMA::~baseDMA() {}
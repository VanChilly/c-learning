// File Name: ABC.cc
// Created by VanChilly on 2022/9/26.
//
#include "ABC.h"
#include <iostream>
#include <cstring>

ABC::ABC(const char *l, int r) {
    label = new char[std::strlen(l) + 1];
    std::strcpy(label, l);
    rating = r;
}

ABC::ABC(const ABC &abc) {
    label = new char[std::strlen(abc.label) + 1];
    std::strcpy(label, abc.label);
    rating = abc.rating;
}

ABC & ABC::operator=(const ABC &abc) {
    if (this == &abc) return *this;
    delete [] label;
    label = new char[std::strlen(abc.label) + 1];
    std::strcpy(label, abc.label);
    rating = abc.rating;
    return *this;
}

std::ostream & operator<<(std::ostream & os, const ABC & abc) {
    os << "Label: " << abc.label << std::endl;
    os << "Rating: " << abc.rating << std::endl;
    return os;
}

void ABC::View() {
    std::cout << "In ABC: " << std::endl;
}

ABC::~ABC() {
    delete [] label;
}

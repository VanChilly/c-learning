// File Name: lacks_dma.h
// Created by VanChilly on 2022/9/26.
//

#ifndef SRC_LACKS_DMA_H
#define SRC_LACKS_DMA_H
#include "ABC.h"
#include "iostream"

class lacksDMA : public ABC{
private:
    enum {COL_LEN = 40};
    char color[COL_LEN];
public:
    lacksDMA(const char * c = "blank", const char * l = "null",
             int r = 0);
    lacksDMA(const char * c, const ABC & ls);
    friend std::ostream & operator<<(std::ostream & os,
            const lacksDMA & ls);
    virtual void View();
};


#endif //SRC_LACKS_DMA_H

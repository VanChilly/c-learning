// File Name: main.cc
// Created by VanChilly on 2022/9/27.
//
#include <iostream>
#include "base_dma.h"
#include "has_dma.h"
#include "string"
#include "lacks_dma.h"

const int CLIENTS = 3;

int main() {
    using std::cin;
    using std::cout;
    using std::endl;

    ABC * p_clients[CLIENTS];
    char label[20];
    int r;
    char kind;
    for ( int i = 0; i < CLIENTS; i ++ ) {
        cout << "Enter label: ";
        cin.getline(label, 20);
        cout << "Enter rating: ";
        cin >> r;
        cout << "Enter 1 for baseDMA or "
        << "2 for lacksDMA or " << "3 for hasDMA:";
        while (cin >> kind && (kind != '1' && kind != '2' && kind != '3'))
            cout << "Enter 1 or 2 or 3: ";
        if (kind == '1')
            p_clients[i] = new baseDMA(label, r);
        else if (kind == '2') {
            char color[40];
            cout << "Enter color: ";
            cin >> color;
            p_clients[i] = new lacksDMA(color, label, r);
        } else {
            char style[40];
            cout << "Enter style: ";
            cin >> style;
            p_clients[i] = new hasDMA(style, label, r);
        }
        while (cin.get() != '\n') continue;
    }
    cout << endl;
    for (auto & p_client : p_clients) {
        p_client->View();
        cout << endl;
    }
    for (auto p_client : p_clients) {
        delete p_client;
    }
    cout << "Done.\n";
    return 0;
}
// File Name: has_dma.h
// Created by VanChilly on 2022/9/27.
//

#ifndef SRC_HAS_DMA_H
#define SRC_HAS_DMA_H

#include "ABC.h"

class hasDMA :public ABC{
private:
    char * style;
public:
    hasDMA(const char * s = "none", const char * l = "null",
           int r = 0);
    hasDMA(const char * s, const ABC & rs);
    hasDMA(const hasDMA & hs);
    ~hasDMA();
    hasDMA & operator=(const hasDMA & hs);
    friend std::ostream & operator<<(std::ostream & os,
            const hasDMA & rs);
    virtual void View();
};
#endif //SRC_HAS_DMA_H

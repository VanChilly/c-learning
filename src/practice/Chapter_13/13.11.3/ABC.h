// File Name: ABC.h
// Created by VanChilly on 2022/9/26.
//

#ifndef SRC_ABC_H
#define SRC_ABC_H
#include <iostream>
class ABC {
protected:
    char * label;
    int rating;
public:
    ABC(const char * l, int r);
    ABC(const ABC & abc);
    virtual ~ABC();
    ABC & operator=(const ABC & abc);
    friend std::ostream & operator<<(std::ostream & os, const ABC & abc);
    virtual void View() = 0;
};
#endif //SRC_ABC_H

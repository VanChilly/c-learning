// File Name: lacks_dma.cc
// Created by VanChilly on 2022/9/26.
//

#include "lacks_dma.h"
#include "cstring"

lacksDMA::lacksDMA(const char *c, const char *l, int r) : ABC(l, r) {
    std::strncpy(color, c, 39);
    color[39] = '\0';
}

lacksDMA::lacksDMA(const char *c, const ABC &ls) : ABC(ls) {
    std::strncpy(color, c, COL_LEN - 1);
    color[COL_LEN - 1] = '\0';
}

std::ostream & operator<<(std::ostream & os, const lacksDMA & ls) {
    os << (const ABC &) ls;
    os << "Color: " << ls.color;
    return os;
}

void lacksDMA::View() {
    std::cout << "In lacksDMA: " << std::endl;
    std::cout << *this << std::endl;
}
// File Name: has_dma.cc
// Created by VanChilly on 2022/9/27.
//

#include "has_dma.h"
#include <iostream>
#include <cstring>

hasDMA::hasDMA(const char *s, const char *l, int r) : ABC(l, r) {
    style = new char[std::strlen(s) + 1];
    std::strcpy(style, s);
}

hasDMA::hasDMA(const char *s, const ABC &rs) : ABC(rs) {
    style = new char[std::strlen(s) + 1];
    std::strcpy(style, s);
}

hasDMA::hasDMA(const hasDMA &hs) : ABC(hs){
    style = new char[std::strlen(hs.style) + 1];
    std::strcpy(style, hs.style);
}

hasDMA::~hasDMA() {
    delete [] style;
}

hasDMA & hasDMA::operator=(const hasDMA &hs) {
    if (this == &hs) return *this;
    ABC::operator=(hs);
    delete [] style;
    style = new char[std::strlen(hs.style) + 1];
    std::strcpy(style, hs.style);
    return *this;
}

std::ostream & operator<<(std::ostream & os, const hasDMA & hs) {
    os << (const ABC &) hs;
    os << "Style: " << hs.style << std::endl;
    return os;
}

void hasDMA::View() {
    std::cout << "In hasDMA: " << std::endl;
    std::cout << *this << std::endl;
}
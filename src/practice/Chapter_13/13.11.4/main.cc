// File Name: main.cc
// Created by VanChilly on 2022/9/27.
//

#include "iostream"
#include "port.h"
#include "vintage_port.h"

#define ll long long
using namespace std;

int main() {
    Port * p[2];
    char brand[40];
    char style[40];
    int bottles;
    char kind;
    for (int i = 0; i < 2; i ++) {
        cout << "Enter brand: ";
        cin >> brand;
        cout << "Enter style: ";
        cin >> style;
        cout << "Enter bottles: ";
        cin >> bottles;
        cout << "Enter kind 1 for Port or 2 for VintagePort: ";
        while (cin >> kind && kind != '1' && kind != '2')
            cout << "Enter 1 or 2: ";
        if (kind == '1') {
            p[i] = new Port(brand, style, bottles);
        } else {
            char nickname[40];
            int year;
            cout << "Enter nickname: ";
            cin >> nickname;
            cout << "Enter year: ";
            cin >> year;
            p[i] = new VintagePort(brand, bottles, nickname, year);
        }
        while (cin.get() != '\n') continue;
    }
    for (auto & item : p) {
        item->Show();
        cout << *item;
        cout << endl;
    }
    cout << "Done!\n";
    return 0;
}
// File Name: vintage_port.cc
// Created by VanChilly on 2022/9/27.
//

#include "vintage_port.h"
#include <iostream>
#include <cstring>

VintagePort::VintagePort() {
    nickname = nullptr;
    year = -1;
}

VintagePort::VintagePort(const char *br, int b, const char *nn, int y)
    : Port(br, "none", b){
    nickname = new char[std::strlen(nn) + 1];
    strcpy(nickname, nn);
    year = y;
}

VintagePort::VintagePort(const VintagePort &vp) : Port(vp) {
    nickname = new char[std::strlen(vp.nickname) + 1];
    strcpy(nickname, vp.nickname);
    year = vp.year;
}

VintagePort & VintagePort::operator=(const VintagePort & vp) {
    if (this == &vp) return *this;
    Port::operator=(vp);
    delete nickname;
    nickname = new char[std::strlen(vp.nickname) + 1];
    strcpy(nickname, vp.nickname);
    return *this;
}

void VintagePort::Show() const {
    Port::Show();
    cout << "nickname: " << nickname << endl;
    cout << "year: " << year << endl;
}

ostream & operator<<(ostream & os, const VintagePort & vp) {
    os << (const Port & ) vp << ", " << vp.nickname << ", " << vp.year << endl;
    return os;
}

VintagePort::~VintagePort() {
    delete [] nickname;
}
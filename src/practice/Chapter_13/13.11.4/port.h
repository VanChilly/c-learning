// File Name: port.h
// Created by VanChilly on 2022/9/27.
//

#ifndef SRC_PORT_H
#define SRC_PORT_H

#include <iostream>

using namespace std;
class Port {
private:
    char * brand;
    char style[20];
    int bottles;
public:
    Port(const char * br = "none", const char * st = "none", int b = 0);
    Port(const Port & p);
    virtual ~Port() {delete [] brand;}
    Port & operator=(const Port & p);
    Port & operator+=(int b);   // adds b to bottles;
    Port & operator-=(int b);   // subtracts b from bottles, if available
    int BottleCount() const { return bottles;}
    virtual void Show() const;
    friend ostream & operator<<(ostream & os, const Port & p);
};


#endif //SRC_PORT_H

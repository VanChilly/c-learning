// File Name: vintage_port.h
// Created by VanChilly on 2022/9/27.
//

#ifndef SRC_VINTAGE_PORT_H
#define SRC_VINTAGE_PORT_H
#include <iostream>
#include "port.h"

class VintagePort : public Port{
private:
    char * nickname;
    int year;
public:
    VintagePort();
    VintagePort(const char * br, int b, const char * nn, int y);
    VintagePort(const VintagePort & vp);
    ~VintagePort();
    VintagePort & operator=(const VintagePort & vp);
    void Show() const;
    friend ostream & operator<<(ostream & os, const VintagePort & vp);
};


#endif //SRC_VINTAGE_PORT_H

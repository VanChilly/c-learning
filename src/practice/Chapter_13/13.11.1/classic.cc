// File Name: classic.cc
// Created by VanChilly on 2022/9/25.
//
#include <cstring>
#include "iostream"
#include "classic.h"

Classic::Classic(char *s1, char *s2, char *s3, int n, double x):
    Cd(s1, s2, n, x) {
    strcpy(work, s3);
}

Classic::Classic(): Cd() {
    strcpy(work, "");
}
void Classic::Report() const {
    Cd::Report();
    std::cout << "Work: " << work << '\n';
}

Classic & Classic::operator=(const Classic &c) {
    Cd::operator=(c);
    strcpy(this->work, c.work);
    return *this;
}

Classic::~Classic() {}
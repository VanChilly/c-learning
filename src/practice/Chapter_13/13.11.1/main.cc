// File Name: main.cc
// Created by VanChilly on 2022/9/25.
//
#include "iostream"
#include "classic.h"

#define ll long long
using namespace std;

void Bravo(const Cd & disk);
int main() {
    Cd c1("Beatles", "Capitol", 14, 35.5);
    Classic c2 = Classic("Pinao Sonata in B flat, Fantasia in C",
                         "Alfred Brendel", "Philips", 2, 57.17 );
    Cd * pcd = &c1;
    cout << "Using object directory:\n";
    c1.Report();    // use Cd method
    c2.Report();    // use Classic method
    cout << "Using type cd * pointer to objects:\n";
    pcd->Report();  // use Cd method for cd object
    pcd = &c2;
    pcd->Report();  // use Classic method for classic object

    cout << "Calling a function with a Cd reference argument:\n";
    Bravo(c1);
    Bravo(c2);

    cout << "Testing assignment: ";
    Classic copy;
    copy = c2;
    copy.Report();
    return 0;
}

void Bravo(const Cd & disk) {
    disk.Report();
}

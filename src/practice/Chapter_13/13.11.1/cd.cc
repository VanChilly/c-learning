// File Name: cd.cc
// Created by VanChilly on 2022/9/25.
//
#include <cstring>
#include <iostream>
#include "cd.h"

Cd::Cd(char *s1, char *s2, int n, double x) {
    strcpy(performers, s1);
    strcpy(label, s2);
    selections = n;
    playtime = x;
}

Cd::Cd(const Cd &d) {
    *this = d;
}

Cd::Cd() {
    strcpy(performers, "");
    strcpy(label, "");
    selections = 0;
    playtime = 0;
}

void Cd::Report() const {
    std::cout << "Performers: " << performers << '\n'
    << "Label: " << label << '\n'
    << "Selections: " << selections << '\n'
    << "Playtime: " << playtime << '\n';
}

Cd & Cd::operator=(const Cd &d) {
    strcpy(performers, d.performers);
    strcpy(label, d.label);
    selections = d.selections;
    playtime = d.playtime;
    return *this;
}

Cd::~Cd() {}


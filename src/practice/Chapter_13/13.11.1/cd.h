// File Name: cd.h
// Created by VanChilly on 2022/9/25.
//

#ifndef SRC_CD_H
#define SRC_CD_H


class Cd {
private:
    char performers[50]{};
    char label[20]{};
    int selections{};     // number of selections
    double playtime{};    // plating time in minutes
public:
    Cd(char * s1, char * s2, int n, double x);
    Cd(const Cd & d);
    Cd();
    virtual ~Cd();
    virtual void Report() const;    // reports all CD data
    Cd & operator=(const Cd & d);
};
#endif //SRC_CD_H

# 13 类继承

## 13.1 一个简单的基类

从一个类派生出另一个类时，原始类称为基类，继承类成为派生类。为说明继承，首先需要一个基类。设计一个简单的TableTennisPlayer类
```C++ 
// tabtenn0.h - - a table-tennis base class
#ifndef TABTENN0_H_
#define TABTENN0_H_
#include <string>
using std::string;
// simple base class
class TableTennisPlayer {
    private:
        string firstname;
        string lastname;
        bool hasTable;
    public:
        TableTennisPlayer (
            const string & fn = "none",
            const string & ln = "none", 
            bool ht = false
            );
        void Name() const;
        bool HasTable() const {return hasTable;};
        void ResetTable(bool v) {hasTable = v;};
};
#endif
```

```C++ 
// tabtenn0.cpp -- simple base-class methods
#include "tabtenn0.h"
#include <iostream>

TableTennisPlayer::TableTennisPlayer (
    const string & fn, 
    const string & ln,
    bool ht
): firstname(fn), lastname(ln), hasTable(ht) {}

void TableTennisPlayer::Name() const {
    std::cout << lastname << ", " << firstname;
}
```
TableTennisPlayer类使用标准string类来存储姓名，相比于使用字符数组，这更方便、更灵活、更安全。其次构造函数使用了第12章介绍的成员初始化列表语法，但也可以像下面这么做：
```C++ 
TableTennisPlayer::TableTennisPlayer (
    const string & fn,
    const string & ln,
    bool ht
) {
    firstname = fn,
    lastname = ln,
    hasTable = ht
}

```
首先将RatedPlayer类声明为从TableTennisClass类声明而来：
```C++ 
// RatedPlayer derives from the TableTennisPlayer base class
class RatedPlayer : public TableTennisPlayer {
    ...
};
```
冒号指出RatedPlayer类的基类是TableTennisPlayer类。上述特生的声明头public表明TableTennisPlayer是一个公有基类，这被称为公有派生。派生类对象包含基类对象，使用公有派生，基类的公有成员将成为派生类的公有成员；基类的私有部分也将成为派生类的一部分，但只能通过基类的公有和保护方法访问。

需要在继承特性中添加什么呢？
- 派生类需要自己的构造函数
- 派生类可以根据需要添加额外的数据成员和成员函数

## 13.1.2 构造函数：访问权限的考虑
派生类不能直接访问基类的私有成员，必须通过基类方法进行访问。派生类构造函数必须使用基类构造函数。

创建派生类对象时，程序首先创建基类对象。从概念上说，这意味着基类对象应当在程序进入派生类构造函数之前被创建。
C++使用成员初始化列表语法来完成这种工作。例如，下面是第一个RatedPlayer构造函数的代码：

```C++
RatedPlayer::RatedPlayer(unsigned int r, const string & fn, const string & ln, bool ht): TableTennisPlayer(fn, ln, ht){
    rating = r;
}
```
其中：TableTennisPlayer(fn, ln, ht) 是成员初始化列表。他是可执行的代码，调用TableTennisPlayer 构造函数。
也可以对派生类成员使用成员初始化列表语法。在这种情况下，应在列表中使用成员名，而不是类名。
```C++
RatedPlayer::RatedPlayer(unsigned int r, const TableTennisPlayer & tp)
    TableTennisPlayer(tp), rating(r){}
```
有关派生类构造函数的要点如下：
- 首先创建基类对象
- 派生类构造函数应通过成员初始化列表将基类信息传递给基类构造函数；
- 派生类构造函数应初始化派生类新增的数据成员。
释放对象的顺序与创建对象的顺序想反，即首先执行派生类的析构函数，然后自动调用基类的析构函数。

> 创建派生类对象时，程序首先调用基类构造函数，然后再调用派生类构造函数。基类构造函数负责初始化继承的数据成员；
> 派生类构造函数主要用于初始化新增的数据成员。派生类的构造函数总是调用一个基类构造函数。可以使用初始化器列表
> 语法指明要使用的基类构造函数，否则将使用默认的基类构造函数。
> 派生类对象过期时，程序将首先调用派生类析构函数，然后再调用基类析构函数。
> 成员初始化列表只能用于构造函数

### 13.1.4 派生类和基类之间的的特殊关系
- 派生类对象可以使用基类的方法，条件是方法不是私有的。
- 基类指针可以在不进行显式类型转化的情况下指向派生类对象；
- 基类引用可以在不进行显式类型转换的情况下引用派生类对象：
```C++
RatedPlayer rplayer1(1140, "Mallory", "Duck", true);
TableTennisPlayer & rt = rplayer;
TableTennisPlater * pt = &rplayer;
rt.Name();
pt->Name();
```
然而，基类指针或引用只能用于调用基类方法，因此，不能使用rt和pt来调用派生类的ResetRanking方法。
通常，C++要求引用和指针类型与赋给的类型匹配，但这一规则对继承来说是例外。然而，这种例外只是单向的，
不可以将基类对象和地址赋给派生类引用和指针。

## 13.2 继承：is-a关系
C++有3种继承方式：公有继承，保护继承，私有继承。
公有继承是最常用的方式，他建立一种is-a关系，即派生类对象也是一个基类对象，可以对基类对象执行的任何操作，
也可以对派生类对象执行。

## 13.3 多态公有继承
方法的行为应取决于调用该方法的对象。这种较复杂的行为称为多态--具有多种形态，即同一个方法的行为随上下文而异。
有两种重要的机制可用于实现多态公有继承：
- 在派生类中重新定义基类的方法。
- 使用虚方法。

经常在基类中将派生类会重新定义的方法声明为虚方法。方法在基类中被声明为虚的后，他在派生类中将自动成为虚方法。
然而，在派生类中使用关键字virtual来指出那些函数是虚函数也是一个好办法。
基类声明一个虚析构函数，这样做是为了确保释放派生对象时，按正确的顺序调用析构函数。为基类声明一个虚析构函数是一种惯例。
> 关键字virtual只用于类声明的方法原型中，而没有用于方法定义。

新关键字virtual 虚方法
如果在基类中没有将方法声明为虚的，则调用时根据指针类型调用对应类的方法；否则根据对象类型调用。
虚函数是C++中用于实现多态(polymorphism)的机制。核心理念就是通过基类访问派生类定义的函数。

假设要同时管理Brass和 BrassPlus账户，如果能使用同一个数组来保存Brass 和 BrassPlus对象，将很有帮助，但这是不可能的。
数组中所有元素的类甩必须相同，而 Brass和 BrassPlus 是不同的类型。然而，可以创建指向Brass的指针数组。
这样，每个元素的类型都相同，但由于使用的是公有继承模型，因此Brass指针既可以指向Brass对象，也可以指向BrassPlus对象。
因此，可以使用一个数组来表示多种类型的对象。这就是多态性。

> 为何需要虚析构函数？
> 如果析构函数不是虚的，则将只调用对应于指针类型的析构函数。否则，将调用相应对象类型的析构函数。
## 13.4 静态联编和动态联编
### 13.4.2 虚成员函数和动态联编
1. 为什么会有两种类型的联编以及为什么默认为静态联编
2. 虚函数的工作原理

编译器处理虚函数的方法是：给每个对象添加一个隐藏成员，隐藏成员中保存了一个指向函数地址数组的指针。这种数组称为虚函数表
（virtual function table）。虚函数表中存储了为类对象进行声明的虚函数的地址。例如，基类对象包含一个指针，该指针指向基类
所有虚函数的地址表。派生类对象将包含一个指向独立地址表的指针。如果派生类提供了虚函数的新定义，该虚函数表将保存新函数的地址；
如果派生类没有重新定义虚函数，该vtbl将保存函数原始版本的地址。如果派生类定义了新的虚函数，则该函数的地址也将被添加到vtbl中。
注意，无论类中包含的虚函数是1个还是10个，都只需要在对象中添加一个地址成员，只是表的大小不同而已。
<img src="assets/虚函数机制.png"/>

调用虚函数时，程序将查看存储在对象中的vtbl地址，然后转向相应的函数地址表。如果使用类声明中定义的第一个虚函数，则程序将使用数组中的第一个
函数地址，并注意性具有该地址的函数。如果使用类声明中的第三个虚函数，程序将使用地址为数组中第三个元素的函数。
总之，使用虚函数时，在内存和执行速度方面由第一定的成本，包括：
- 每个对象都将增大，增大量为存储地址的空间；
- 对于每个类，编译器都创建一个虚函数地址表（数组）；
- 对于每个函数调用，都需要执行一项额外的操作，即到表中查找地址。

### 13.4.3 有关虚函数注意事项
虚函数的一些要点
- 在基类方法的生命中使用关键字virtual可使该方法在基类以及所有的派生类（包括从派生类派生出来的类）中是虚的。
- 如果使用指向对象的引用或指针来调用虚方法，程序将使用为对象类型定义的方法。而不使用为引用或指针类型定义的方法。这称为动态联编或晚期联编，
这种行为非常重要，因为这样基类指针或引用可以指向派生类对象。
- 如果定义的类将被用作基类，则应将那些要在派生类中重新定义的类方法声明为虚的。

1. 构造函数不能是虚函数。
2. 析构函数应当是虚函数，除非类不用做基类。（给类定义一个虚析构函数并非错误），即使这个类不用做基类；这只是一个效率方面的问题。
3. 友元不能是虚函数，因为友元不是类成员。如果由于这个原因引起设计问题，可以通过让友元函数使用虚成员函数来解决。
4. 如果派生类没有重新定义函数，将使用该函数的基类版本。如果派生类位于派生链中，则将使用最新的虚函数版本，例外的情况是基类版本是隐藏的。
5. 重新定义继承的方法并不是重载，如果在派生类中重新定义函数，将不是使用相同的函数特征标覆盖基类声明，而是隐藏同名的基类方法，不管参数特征标
如何。
第一，如果重新定义集成的方法，应确保与原来的原型完全相同，但如果返回类型是基类引用或指针，则可以修改为指向派生类的引用或指针。这种特性被称为
返回类型协变（covariance of return type），因为允许返回类型随类的类型的变化而变化。
```C++
class Dwelling {
    public:
        virtual Dwelling & build(int n);
        ...
};
class Hovel: public Dwelling {
    public:
        virtual Hovel & build(int n);
};
```
第二，如果基类声明被重载了，则应在派生类中重新定义所有的基类版本。
## 13.5 访问控制: protected
关键字protected 与private相似，在类外只能用公有类成员来访问protected部分中的类成员。private和protected
之间的区别只有在基类派生的类中才会表现出来。派生类的成员可以直接访问基类的保护成员，但不能直接访问基类的私有成员。
因此，对于外部世界来说，保护乘员的行为与私有成员相似；但对于派生类来说，保护乘员的行为与共有成员相似。
> 最好对累数据成员采用私有访问控制，不要使用保护访问控制；同时通过基类方法使派生类能够访问基类数据。
## 13.6 抽象基类 （abstract base class, ABC）
纯虚函数(pure virtual function)提供未实现的函数，声明的结尾处为=0
```C++ 
virtual double Area() const = 0;
```
当类声明中包含纯虚函数时，则不能创建该类的对象。

## 13.7 继承和动态内存分配
### 13.7.2 派生类使用new
当基类和派生类都采用动态内存分配时，派生类的析构函数、复制构造函数、赋值运算符都必须使用相应的基类方法来处理基类元素。
- 对于析构函数，这是自动完成的
- 对于构造函数，这是通过在初始化成员列表中调用基类的复制构造函数来完成的；
- 如果不这样做，将自动调用基类的默认构造函数。对于复制运算符，这是通过使用作用域解析运算符显式地调用基类的赋值运算符来完成的。

### 13.8.3 公有继承的考虑因素
7. 友元函数
由于友元函数并非类成员，因此不能继承。然而，可以通过强制类型转换将派生类引用或指针转换为基类引用或指针，
然后使用转换后的指针或引用来调用基类的友元函数。也可以使用第15章将讨论的运算符dynamic_cast<>来进行强制类型转换；
```C++
os << dynamic_cast<cosnt baseDMA &> (hs);
```

## 13.9 总结
继承通过使用已有的类（基类）定义新的类（派生类），使得能够根据需要修改编程代码。公有继承建 立 is-a关系，
这意味着派生类对象也应该是某种基类对象。作为is-a模型的一部分，派生类继承基类的数 据成员和大部分方法，
但不继承基类的构造函数、析构函数和赋值运算符。派生类可以直接访问基类的公 有成员和保护成员，
并能够通过基类的公有方法和保护方法访问基类的私有成员。可以在派生类中新增数 据成员和方法，还可以将派生类用作基类，来做进一步的幵发。
每个派生类都必须有自己的构造函数。程序创建派生类对象时，将首先调用基类的构造函数，然后调用派生类的构造函数；程序删除对象时吗，
将首先调用派生类的析构函数，然后调用基类的析构函数。 如果要将类用作基类，则可以将成员声明为保护的，而不是私有的，这样，派生类将可以直接访问这
些成员。然而，使用私有成员通常可以减少出现编程问题的可能性。如果希望派生类可以重新定义基类的 方法，则可以使用关键字virtual将它声明为虚的。
这样对于通过指针或引用访问的对象，能够根据对象类 型来处理，而不是根据引用或指针的类型来处理。具体地说，基类的析构函数通常应当是虚的。
可以考虑定义一个ABC: 只定义接口，而不涉及实现。例如，可以定义抽象类Shape, 然后使用它派生出具体的形状类，如 Circle和 Square。
ABC 必须至少包含一个纯虚方法，可以在声明中的分号前面加h=0 来声明纯虚方法。
```C++
virtual double area() const = 0；
```
不一定非得定义纯虚方法。对于包含纯虚成员的类，不能使用它来创建对象。纯虚方法用于定义派生类的通用接口。
